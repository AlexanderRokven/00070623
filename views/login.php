<?php
/**
 * Login.php - renders a login form
 * 
 * @author Bugslayer
 * 
 */
?>
<form id="@@project" method="post" action="index.php?action=login&page=login">
	<table style="width:450px; margin-left: auto; margin-right: auto">
		<tr>
			 <td valign="top">
			  	<label for="userid">Gebruikersnaam</label>
			 </td>
			 <td valign="top">
			  	<input  type="text" name="userid" maxlength="50" size="30">
			 </td>
		</tr>
		<tr>
			 <td valign="top">
			  	<label for="password">Wachtwoord</label>
			 </td>
			 <td valign="top">
			  	<input  type="password" name="password" value="" maxlength="50" size="30">
			 </td>
		</tr>
		<tr>
			 <td valign="top"></td>
			 <td valign="top">
			  	<input type="submit" name="commit" value="Login">
			 </td>
		</tr>
	</table>	
</form>

<!-- html voor @@project -->
