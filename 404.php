<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Fa Reijnders</title>

		<meta charset="utf-8">
		<meta name="description" content="Casustoets Webwereld">
		<meta name="author" content="waar0003">

	    <link rel="shortcut icon" href="favicon.ico">		

		<link rel="stylesheet" type="text/css" href="css/homestyle.css"/>
		
		<!-- javascripts laden -->
		<script src="lib/jquery-2.0.3.min.js"></script>
		<script src="js/scripts.js"></script>
	</head>

	<body>
		<header>
			<div id="account">
				<?php include('components/account.php'); ?>
			</div>
		</header>
		<nav>
			<?php include('components/menu.php'); ?>
		</nav>
<h1>404 – File or Directory not found</h1>
<p>
	De opgevraagde pagina bestaat niet. Dit kan doordat 
	<ol>
	<li> je als gebruiker stiekem met de queryvars in de URL hebt geknoeid</li>
	<li> er in het menu verwezen wordt naar een artikel dat door de admin net is weggehaald. </li>
	<li>Ook zou er een programmeerfout in het menu kunnen zitten.</li>
	</ol>
</p>
</body>
</html>