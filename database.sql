-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- ---------------------------------------------------------
--
-- LET OP: ZELF DE DATABASE EN GEBRUIKER AANMAKEN !!!!!!!!!
--         DIT SCRIPT DOET DAT NIET VOOR JE
--
-- ---------------------------------------------------------
CREATE DATABASE IF NOT EXISTS `project`
USE `project`
-- Structuur van  tabel project.article wordt geschreven
CREATE TABLE IF NOT EXISTS `article` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Content` mediumtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel project.article: ~5 rows (ongeveer)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`ID`, `Name`, `Content`) VALUES
	(2, 'Over Ons', '<p>We zijn een uitersd professionele ogranisatie. Maak kennis met ons vakundig personeel</p>'),
	(3, 'Lorem Ipsum', '<img src="img/loremipsum.jpg" alt="Lorem Ipsum" style="float:right;">							\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum porta mi, at laoreet justo pharetra vel. Proin at risus id nibh molestie feugiat. Phasellus felis nunc, aliquam non tristique in, pharetra a tortor. Quisque ut dapibus tellus. Maecenas non risus bibendum, hendrerit leo sed, hendrerit tortor. Cras non risus mollis, tempus arcu nec, volutpat lorem. Nam eget sollicitudin est. Proin euismod volutpat risus id consequat. Phasellus id nunc ex. Nulla massa nisi, dignissim ac metus et, ultricies rhoncus ex. Fusce eget velit a metus vehicula ornare.\r\n</p><p>Quisque porttitor magna viverra est lobortis scelerisque. Pellentesque nec ligula vitae libero mollis efficitur. Quisque quis dui non neque dapibus egestas. Sed blandit nulla felis. Phasellus dignissim metus sit amet dui cursus tincidunt. Sed hendrerit magna vitae libero lobortis, sed commodo velit rutrum. Proin luctus imperdiet mauris, ut dapibus enim volutpat nec. Suspendisse dapibus fermentum sem vitae imperdiet.\r\n</p><p>Sed tempor, odio nec laoreet sagittis, turpis arcu ultricies metus, sodales rhoncus nisi justo nec velit. Integer placerat libero sit amet magna pretium efficitur. Ut facilisis est ac risus iaculis suscipit. Curabitur aliquam nulla et leo commodo finibus. Vivamus sapien libero, facilisis vel aliquam ut, congue et ipsum. Pellentesque eu purus nec neque sodales cursus. Phasellus faucibus mi sed ligula placerat, at efficitur massa aliquet. In eu quam euismod, dapibus nulla tristique, malesuada ligula. Cras eget quam ut dolor molestie porta. Sed quam diam, tincidunt quis tempus ac, dictum et nisi. Nulla facilisi. Quisque finibus commodo mauris, id elementum quam scelerisque vel.\r\n</p><p>Cras tincidunt maximus ante. Praesent vehicula ligula vel urna mattis sodales ut in quam. Proin vitae enim venenatis, placerat quam vel, scelerisque nunc. Suspendisse potenti. Phasellus lorem leo, venenatis id tempus quis, auctor nec diam. Fusce nec ex dapibus mauris elementum bibendum non id nulla. Donec porttitor sapien arcu, at tempus ligula mattis ut. Integer nec auctor metus.\r\n</p><p>Duis quis cursus magna, eu volutpat elit. Sed tempor finibus tellus ut fringilla. Praesent luctus quis elit vitae vulputate. Duis mollis imperdiet nulla eget ultricies. Donec in lacus ex. In a viverra velit, eu vulputate purus. Nulla pellentesque leo ac sem malesuada iaculis. Praesent congue mi vel ultrices mattis. Quisque finibus purus in metus lacinia vestibulum. Donec cursus iaculis est, nec ullamcorper arcu hendrerit a. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec eget magna ullamcorper leo auctor vulputate eu vel libero. Nunc at mattis augue, non malesuada lectus. Donec sed blandit est, vel fringilla ante. Cras a justo cursus, ultricies metus sit amet, sodales elit.</p>'),
	(4, 'Slevels', '<img src="articles/loremipsum.jpg" alt="Lorem Ipsum" style="position:relative; float:right;">\r\n<p>Geen enkel hi-tech bedrijf kan nog zonder goede slevels. Daarom maken wij ons sterk om de beste slevels \r\nter wereld te leveren. Onze inkopers speuren de hele wereld af naar de hoogste kwaliteit materialen. Samen \r\nmet het vakmanschap van onze slevelaars ontstaat een product wat in de wereld zijn weerga niet kent in \r\ndesign en gebruik.</p>\r\n<p>De @@project Research & Development afdeling heeft experts op het gebied van materiaaltechnologie. Onze \r\nmedewerkers zijn veelal onderzoekers en technici bij universiteiten, onderzoeksinstituten en de industrie, \r\ndie er behagen in scheppen om kennis over de verwerking en toepassing van materialen te verspreiden of te \r\nabsorberen, binnen en buiten het materialenveld.</p>\r\n<p>Onze slevelaars zijn niet zomaar mensen die zich bekwaamd hebben in een of slechts enkele activiteiten \r\nbinnen het slevelen. Zij verstaan hun vak zoals het heet in het spraakgebruik. Dat kan zijn het \r\nhandmatig en/of machinaal maken van onderdelen. De meeste van de daarvoor benodigde vaardigheden hebben ze in \r\nde praktijk geleerd. In de opleidingsperiode volgt zo iemand vaak een dag- of dagdeel theorie aan een ROC of \r\neen ander opleidingsinstituut ter ondersteuning van zijn vakkennis en vaardigheden.</p>\r\n<p>Het beroep van slevelen wordt al eeuwen uitgeoefend. In het verleden werd dan vaak gesproken van ambachtsslevelaar. \r\nDenk in dit verband aan de ambachtscholen van jaren terug. Deze stonden bij veel mensen niet in hoog aanzien. \r\nTegenwoordig zit het bedrijfsleven te \'springen\' om mensen met een soortgelijke opleiding. Men zegt dan ook vaak: \r\n\'Wat een vakman!\'. In de tijd van de gilden kwamen jongelui bij een meester (bijvoorbeeld een meesterslevelaar) in de \r\nleer. Vervolgens kon men daar gezel worden en als de meester niet te benauwd was voor concurrentie, uiteindelijk meesterslevelaar.</p>'),
	(5, 'Wigbekken', '<img src="articles/Carbon_Nanotubes.png" alt="carbon nanotubes" style="position:relative; float:right;">\r\n<p>Voor onze Wigbekken is onze research & development afdeling continu op zoek naar nieuwe high-tech materialen, \r\nzoals carbon composites met nano tubes. Hiermee wordt een product verkregen dat in staat is de concurrentie te lijf \r\nte gaan.</p>\r\n<p>Voor onze composieten gebruiken we Meerwandige koolstofnanobuizen (MWCNT = Multi-Wall Carbon Nanotube). Ze bestaan \r\nuit meerdere lagen opgerold grafeen en hebben een grotere diameter dan SWCNT (Single-Wall Carbon Nanotube), typisch \r\nzo\'n 5-100 nm. De afstand tussen twee lagen in een MWCNT is ongeveer gelijk aan de afstand tussen twee lagen grafeen \r\nin grafiet: zo\'n 0.33 nm. Bij @@project passen wij vrijwel altijd MWCNT toe vanwege de verhoogde weerstand tegen chemicalien. \r\nMWCNT kunnen gefunctionaliseerd worden (er kunnen andere moleculen aan het oppervlak gebonden worden voor extra \r\nfunctionaliteiten) zonder dat de structuur verloren gaat.</p>'),
	(6, 'Zwenkmoeren', '<p>Door continu ergonomisch onderzoek in combinatie met het beste koper en vlas leveren wij Zwenkmoeren die u tijd gaan besparen.</p>');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;


-- Structuur van  tabel project.message wordt geschreven
CREATE TABLE IF NOT EXISTS `message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name_first` varchar(50) NOT NULL,
  `Name_middle` varchar(50) DEFAULT NULL,
  `Name_last` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telephone` varchar(50) DEFAULT NULL,
  `Adress_street` varchar(50) DEFAULT NULL,
  `Adress_number` varchar(50) DEFAULT NULL,
  `Adress_zipcode` varchar(50) DEFAULT NULL,
  `Adress_city` varchar(50) DEFAULT NULL,
  `Subject` varchar(512) NOT NULL,
  `Message` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel project.message: ~2 rows (ongeveer)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`ID`, `Name_first`, `Name_middle`, `Name_last`, `Email`, `Telephone`, `Adress_street`, `Adress_number`, `Adress_zipcode`, `Adress_city`, `Subject`, `Message`) VALUES
	(1, 'Daan', 'de', 'Waard', 'daanwaard@gmail.com', '+31623016807', 'Jan van Hoofkwartier', '52', '4333 EB', 'Middelburg', 'Dit is een test', 'Wat een bericht'),
	(2, 'Hans', '', 'Pans', 'a@b.nl', '0344232334', 'gdfggdf', '12', 'dfgdfg', 'gdfgd', 'dfwedfasdf', 'sdafasdfdsa gagar gargrg adrger gaer gtyu65et ueu w yrehhtrsjtr jaykqkj QKKJrhhT  6TRTTY R YKEI AY WY');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- Structuur van  tabel project.order wordt geschreven
CREATE TABLE IF NOT EXISTS `order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` varchar(50) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Slevels` int(11) NOT NULL DEFAULT '0',
  `Wigbekken` int(11) NOT NULL DEFAULT '0',
  `Zwenkmoeren` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FK_order_user` (`CustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel project.order: ~1 rows (ongeveer)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;


-- Structuur van  tabel project.user wordt geschreven
CREATE TABLE IF NOT EXISTS `user` (
  `Userid` varchar(50) NOT NULL,
  `Password` varchar(60) NOT NULL,
  `Name_first` varchar(50) NOT NULL,
  `Name_middle` varchar(50) DEFAULT NULL,
  `Name_last` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Telephone` varchar(50) DEFAULT NULL,
  `Adress_street` varchar(50) DEFAULT NULL,
  `Adress_number` varchar(50) DEFAULT NULL,
  `Adress_zipcode` varchar(50) DEFAULT NULL,
  `Adress_city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel project.user: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`Userid`, `Password`, `Name_first`, `Name_middle`, `Name_last`, `Email`, `Telephone`, `Adress_street`, `Adress_number`, `Adress_zipcode`, `Adress_city`) VALUES
	('admin', 'qw75ElNTVyllI', 'Daan', 'de', 'Waard', 'daan.de.waard@hz.nl', '0118 489190', 'Edisonweg', '4', '4380 AJ', 'Vlissingen'),
	('dfg', 'qw75ElNTVyllI', 'Mischa', 'de', 'Boer', 'm.de.boer@hz.nl', '', '', '', '', ''),
	('hjk', 'qw75ElNTVyllI', 'Bauke', '', 'Bil', 'b.bil@hz.nl', '', '', '', '', ''),
	('pas', 'qw75ElNTVyllI', 'Daan', '', 'Beckers', 'd.beckers@hz.nl', '', '', '', '', ''),
	('qwe', 'qw75ElNTVyllI', 'Anton', '', 'Everse', 'a.everse@hz.nl', '', '', '', '', ''),
	('rty', 'qw75ElNTVyllI', 'Ruth', 'de', 'Waard', 'r.de.waard@hz.nl', '', '', '', '', ''),
	('uio', 'qw75ElNTVyllI', 'Wouter', 'de', 'Vijlder', 'w.de.vijlder@hz.nl', '', '', '', '', '');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
project