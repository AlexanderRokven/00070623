/**
 * Search.js - does some test stuff.
 * 
 * @author still nobody
 * 
 */
$(document).ready(function() {
	var timer;
	$("#search_text").keyup(function(){
		clearTimeout(timer);
		timer = setTimeout(search,300);
	});
function search () {
	// Zet POST data uit search_text om naar variabel
	var search = $("#search_text").val();
	// Check of de search_text ingevuld is/wordt
	if (search !== "") {
		$('#resultlist').html('Checking...');
	}
	else {
		$('#resultlist').html('');
	}
	// Tijd om te checken. Hiervoor gebruiken we AJAX omdat we geen server nodig willen hebben.
 $.ajax ({
 	// Zet de variable search om naar Search.
 	data: 	{
 		Search: search
 			}, 
 		type: 'POST',
 	// Roep het php bestand op waarin de database kijkt.
 		url: '/components/search.php',
 	// Als dit klopt, ga dan het volgende doen:
 		success: function (data) {
 			if (data ==  'Sorry, er is niets gevonden.') {

 			} else {
 			}
 	// Zet in de span met het ID usernameResult de tekst uit (data)
 			$('#resultlist').html(data);
 			}
 		});
 	}
});
